var agentManABI = require('../abi/AgentManagement.js')
var stakingABI = require('../abi/Staking.js')
var hostManagementABI = require('../abi/HostManagement.js')
var agentsTreasuryABI = require('../abi/AgentsTreasury.js')
var deploymentRequestsABI = require('../abi/DeploymentRequests.js')
var workABI = require('../abi/WorkToken.js')
var shardModifABI = require('../abi/ShardModifications.js')
var agentVersioningABI = require('../abi/AgentVersioning.js')
var assigningJobsABI = require('../abi/AssigningJobs.js')
var permissionsABI = require('../abi/EphemeralPermissions.js')

/**
* WorkToken, Staking, HostManagement, AgentManagement, AgentVersioning, AgentsTreasury,
* ShardModifications, DeploymentRequests, AssigningJobs, EphemeralPermissions,
* AgentObligations
**/

var contractAddrs = [

  "0x2cE224CaD729c63C5cDF9CE8F2E8B5B8f81eC7B4",
  "0xAb119259Ff325f845F8CE59De8ccF63E597A74Cd",
  "0xa6126AD8B8307C6e1b668F486BEA155e814FA22d",
  "0x9E05B78ea853a4B093694645561c4BFc953A6f62",
  "0x7277c23a5211eA4F749a37cB8D79AB0620BFA740",
  "0x1224d12E835cbDbcaE121b29EfAB444b66262441",
  "0xcCAD3CB4B2A2cb917B2c6eB4025C78d4cAdeb804",
  "0x62E4996bE6558e110AF1B112E6Ef878d2349325f",
  "0x4B74c60839a2019614274Db683A8DA4eF0F3835f",
  "0xBEd272E0eee0F8DA9397A71237e5535F24FB4F87",
  "0x722f65fA2fA779717dC9dE57200b7C67316dA93e"

];

var contractDict = {

  "workToken": [workABI.workToken, contractAddrs[0]],
  "staking": [stakingABI.staking, contractAddrs[1]],
  "hostManagement": [hostManagementABI.hostManagement, contractAddrs[2]],
  "agentManagement": [agentManABI.agentManagement, contractAddrs[3]],
  "agentVersioning": [agentVersioningABI.agentVersioning, contractAddrs[4]],
  "agentTreasury": [agentsTreasuryABI.agentsTreasury, contractAddrs[5]],
  "shardModifications": [shardModifABI.shardModif, contractAddrs[6]],
  "deploymentRequests": [deploymentRequestsABI.deployments, contractAddrs[7]],
  "assigningJobs": [assigningJobsABI.assigningJobs, contractAddrs[8]],
  "ephemeralPermissions": [permissionsABI.ephemeral, contractAddrs[9]],
  "agentObligations": [undefined, contractAddrs[10]]

};

module.exports = {

  contractDict

}
