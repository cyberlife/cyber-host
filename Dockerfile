FROM node:8

# Create app directory
WORKDIR /usr/src/host_one

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install

# Bundle app source
COPY . .

EXPOSE 38045

CMD node ./host/host.js --storage='./host1Bots/' --port=38045 --ip='127.0.0.1' --cli='node_modules/cyber-host-cli/bin/run' --priv='' --pbk='ed9d02e382b34818e88b88a309c7fe71e65f419d' --id=1
