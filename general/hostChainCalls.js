var chain = require('../chain/chainInteraction')
const BigNumber = require('bignumber.js')
var sha3 = require('ethereumjs-util').sha3
const MerkleTree = require('merkletreejs')
const crypto = require('crypto')

var web3 = chain.web3
var responseWindow = 5

var utils = require('./utils')
var ipfsUtils = require('./ipfsInteraction')

async function fundAgent(pubKey, name, funds) {

  var web3 = chain.web3

  var personalFunds = await web3.eth.getBalance(pubKey)

  if (personalFunds >= funds)
    chain.fundAgent(pubKey, name, funds)

  else console.log("You don't have that much money in your account")

}

async function deleteAgent(pbk, name) {

  var agentExists = await chain.getNameAvailability(name)

  if (agentExists == true) {

    await chain.deleteAgent(pbk, name)

  } else console.log("The agent with the name '" + name + "' does not exist")

}

async function getCurrentAgentShard(name, callback) {

  var canGetShard = await chain.agentIsRegistered(name)

  if (canGetShard != undefined && canGetShard == true) {

    var ipfsAddress = await chain.getAgentRoot(name)

    ipfsCalls.getIPFSData(ipfsAddress[2].toString(), async function(err, sh) {

      if (err != undefined && err != null) return callback(err, undefined)

      return callback(undefined, sh[0].content)

    })

  } else {

    return callback(undefined, undefined)

  }

}

async function getHostPrices(stakedIds) {

  var hostPrices = []

  for (var i = 0; i < stakedIds.length; i++) {

    var price = await chain.getHostRent(stakedIds[i].id.toB58String())

    hostPrices.push(new BigNumber(price))

  }

  return hostPrices

}

async function changeHostsTrees(pubKey, name, hostsIds, changeType) {

  var hostAddr = ''
  var agentsTree = ''
  var i
  var currentId

  console.log('Changing hosts trees...')

  for (i = 0; i < hostsIds.length; i++) {

    if (changeType == 0) currentId = hostsIds[i].id.toB58String()
    else if (changeType == 1) currentId = hostsIds[i]

    hostAddr = await chain.getHostAddrFromID(currentId)

    agentsTree = await chain.getAgentsTree(hostAddr)

    if ( agentsTree[0] == undefined || agentsTree[0] == NaN
        || agentsTree[0].length == 0
      ) {

      if (changeType == 0) {

        initAgentTree(pubKey, name, hostAddr, currentId)

      }

    } else {

      updateAgentTree(pubKey, name, hostAddr, currentId, agentsTree[0], changeType)

    }

  }

}

async function initAgentTree(pubKey, name, hostAddr, currentId) {

  await ipfsCalls.postDatatoIPFS(Buffer.from(name, 'utf8'), async function(err, postResult) {

    await chain.setAgentsTree(pubKey, hostAddr, postResult[0].hash.toString(), sha3(name), 1)

    var tree = await chain.getAgentsTree(hostAddr)

  })

}

async function updateAgentTree(pubKey, name, hostAddr, currentId, treeIpfs, changeType) {

  //BUGS: agents are not deleted from hosts trees

  await ipfsCalls.getIPFSData(treeIpfs, async function(err, leaves) {

    var splitLeaves = leaves[0].content.toString().split('\n')

    var agentsNumber = await chain.getAgentsCount(currentId)

    if (changeType == 0) {splitLeaves.push(name); agentsNumber++}

    else {splitLeaves = await utils.deleteElementFromArray(name,
        splitLeaves, "string"); agentsNumber--}

    if (agentsNumber == 0) {

      await chain.setAgentsTree(pubKey, hostAddr, "", [], 0)

      return

    } else if (agentsNumber > 0) {

      var hashedLeaves = await utils.getHashedLeaves(splitLeaves)

      const tree = new MerkleTree(hashedLeaves, sha3)

      var bufferedLeaves

      for (i = 0; i < splitLeaves.length; i++) {

        if (i == 0) bufferedLeaves = Buffer.from(name + '\n', 'utf8')

        else if (i > 0 && i < splitLeaves.length - 1)
          bufferedLeaves = bufferedLeaves.concat(Buffer.from(name + '\n', 'utf8'))

        else bufferedLeaves = bufferedLeaves.concat(Buffer.from(name, 'utf8'))

      }

      await ipfsCalls.postDatatoIPFS(bufferedLeaves, function(err, postRes) {

        chain.setAgentsTree(pubKey, hostAddr, postRes[0].hash.toString(),
          tree.getRoot(), agentsNumber)

      })

    }

  })

}

async function respondToRequest(pbk, personalNode, name, position) {

  var deploymentsLenght = await chain.getDeploymentsLength(name)

  if (deploymentsLenght - 1 != position) return

  var deploymentRoot = await chain.getDeploymentHostsRoot(name, position)

  var ipfsProof = await chain.getDeploymentIPFSProof(name, position)

  var cannotRespond = await chain.hasEnoughResponses(name, position)

  var nameRegistered = await chain.getNameAvailability(name)

  if (nameRegistered == false) return

  if (cannotRespond == false && deploymentRoot != undefined && ipfsProof != undefined) {

    ipfsUtils.getIPFSData(ipfsProof.toString(), async function(err, ipfsData) {

      if (err != null || ipfsData == undefined || ipfsData == null) return

      var ipfsContent = ipfsData[0].content

      //TODO: check that deployer specified enough money to be deposited to cover all hosts

      if (ipfsContent.length % 32 != 0 || ipfsContent.length == 0) return

      var leaves = await utils.splitBuffer(ipfsContent)

      var personalIdHash = sha3(personalNode.peerInfo.id.toB58String())

      var personalExists = await utils.findElementInArray(personalIdHash.toString('hex'), leaves.map(e => e.toString('hex')))

      if (personalExists == true) {

        const hostsTree = new MerkleTree(leaves, sha3)

        var proof = hostsTree.getProof(personalIdHash)

        if (proof.length == 0 && hostsTree.getLeaves().length == 1
          && hostsTree.getLeaves()[0].toString('hex') == personalIdHash.toString('hex')) {

            chain.respondToRequest(pbk, name, position, hostsTree.getLeaves()[0])

        } else if (proof.length > 0) {

          var concatenatedProof = await utils.concatProof(proof)

          chain.respondToRequest(pbk, name, position, concatenatedProof)

        }

      }

    })

  }

}

async function voteModification(pbk, name) {

  var modifIntData = await chain.getModificationIntData(name)

  var currentAgentTree = await chain.getAgentRoot(name)

  var votingWindow = await chain.getVotingWindow()

  var modifTree = await chain.getModificationTree(name)

  var modifArrays = await chain.getModificationArrays(name)

  var currentDate = new BigNumber(Math.floor(Date.now() / 1000))

  if (modifIntData[0] + votingWindow - responseWindow <= currentDate) return

  var i = 0

  ipfsUtils.getIPFSData(currentAgentTree[2], async function(err1, currentBillings) {

    ipfsUtils.getIPFSData(modifTree[1], async function(err2, modifBillings) {

      if (err1 != undefined || err2 != undefined) return

      var pastLeaves = currentBillings[0].content.toString('utf8').split('\n')

      var newLeaves = modifBillings[0].content.toString('utf8').split('\n')

      var i = 0

      if (modifIntData[1] == 0) {

        //Check that past leaves are present in the current leaves in ShardModifications

        for (i = 0; i < pastLeaves.length; i++) {

          if (pastLeaves[i].length > 2 && pastLeaves[i] != '' && pastLeaves[i] != ' ') {

            if (pastLeaves[i] != newLeaves[i]) {chain.voteModification(pbk, name, false); return}

          }

        }

      } else if (modifIntData[1] == 1) {

        for (i = 0; i < pastLeaves.length; i++) {

          for (var j = 0; j < modifArrays[0].length; j++) {

            if (pastLeaves[i].toLowerCase() == modifArrays[0][j].toLowerCase()) {

              chain.voteModification(pbk, name, false);

              return

            }

          }

        }

      } else {

        chain.voteModification(pbk, name, false);

        return

      }

      //TODO: check that, in case of deployment, the deployer will send enough money to cover all hosts

      //Create tree from new leaves and compare the root of the tree with the root from the SC

      var hashedLeaves = await utils.getHashedLeaves(newLeaves)

      var billingTree = new MerkleTree(hashedLeaves, sha3)

      var billingRoot = await billingTree.getRoot().toString('hex')

      if (('0x' + billingRoot) != modifTree[0]) {console.log("false 4"); chain.voteModification(pbk, name, false); return}

      chain.voteModification(pbk, name, true);

    })

  })

}

module.exports = {

  respondToRequest,
  voteModification,

  fundAgent,
  deleteAgent,
  getCurrentAgentShard,
  getHostPrices,
  changeHostsTrees

}
