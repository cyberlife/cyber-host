const BigNumber = require('bignumber.js');

function quickSort(arr, altArrays, left, right, intType){

   if (intType != 1 && intType != 2) return undefined

   if (altArrays != undefined && altArrays.constructor === Array && altArrays.length > 0) {

     for (var j = 0; j < altArrays.length; j++) {

        if (altArrays[j].length != arr.length) return undefined

     }

   }

  var len = arr.length,
      pivot,
      partitionIndex;

  if(left < right){

    pivot = right;
    partitionIndex = partition(arr, altArrays, pivot, left, right, intType);

   //sort left and right
   quickSort(arr, altArrays, left, partitionIndex - 1, intType);
   quickSort(arr, altArrays, partitionIndex + 1, right, intType);

  }

  if (altArrays.constructor === Array && altArrays.length > 0) return [arr, altArrays]

  return arr;

}

function partition(arr, altArrays, pivot, left, right, intType) {

   var pivotValue = arr[pivot],
       partitionIndex = left;

   for(var i = left; i < right; i++) {

    if (intType == 2) {

      if(arr[i].isLessThan(pivotValue)) {

        swap(arr, altArrays, i, partitionIndex, intType);
        partitionIndex++;

      }

    } else if (intType == 1) {

      if(arr[i] < pivotValue) {

        swap(arr, altArrays, i, partitionIndex, intType);
        partitionIndex++;

      }

    } else return

  }

  swap(arr, altArrays, right, partitionIndex, intType);

  return partitionIndex;

}

function swap(arr, altArrays, i, j, intType) {

   var temp = arr[i];
   arr[i] = arr[j];
   arr[j] = temp;

   if (altArrays != undefined && altArrays.constructor === Array && altArrays.length > 0) {

     for (var p = 0; p < altArrays.length; p++) {

       var aux = altArrays[p][i]
       altArrays[p][i] = altArrays[p][j]
       altArrays[p][j] = aux

     }

   }

}

module.exports = {

  quickSort

}
